extends Area2D

export (float) var animation_speed = 0.1

onready var tween = $Tween

onready var ray = $RayCast2D

onready var animated_sprite = $AnimatedSprite

onready var explode_sound = $Explode

var moving = false

var saved_direction = Vector2.RIGHT

var directions = {"right": Vector2.RIGHT,
					"left": Vector2.LEFT,
					"down": Vector2.DOWN,
					"up": Vector2.UP}

# Called when the node enters the scene tree for the first time.
func _ready():
	var player = get_parent().get_node("Player")
	player.connect('moved', self, '_on_Player_moved')
	_move()
	print(animated_sprite)

func _on_Player_moved(direction):
	_move()

func _move():
	var next_tile = saved_direction * Global.tile_size
	ray.cast_to = next_tile
	ray.force_raycast_update ()
	if ray.get_collider() != null:
		if ray.get_collider().get_collision_layer() == 16:
			explode_sound.play()
			animated_sprite.play('death')
			return
	tween.interpolate_property (self, 'position', position, position + next_tile, animation_speed, Tween.TRANS_CIRC,Tween.EASE_IN_OUT)
	tween.start()

func _get_direction (direction):
	saved_direction = directions[direction]
	if direction == 'right':
		$AnimatedSprite.flip_h = false
	if direction == 'left':
		$AnimatedSprite.flip_h = true

func _on_Bullet_body_entered(body):
	if body.get_collision_layer() == 2:
		body._die()
		queue_free()
	pass # Replace with function body.


func _on_AnimatedSprite_animation_finished():
	if animated_sprite.animation != 'idle':
		queue_free()
	pass # Replace with function body.
