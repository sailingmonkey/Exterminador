extends KinematicBody2D

signal moved(direction)

export (float) var animation_speed = 0.2

export (int) var health = 3

export (PackedScene) var bullet_scene

var inputs = {"right": Vector2.RIGHT,
					"left": Vector2.LEFT,
					"down": Vector2.DOWN,
					"up": Vector2.UP}

onready var ray = $RayCast2D

onready var tween = $Tween

onready var animation_player = $AnimationPlayer

onready var sprite = $AnimatedSprite

onready var walk_sound = $Walk

onready var shoot_shound = $Shoot

onready var hit_sound = $Hit

onready var die_sound = $Die

var future_direction = null

var facing_direction = 'right'
	
var can_shoot = true

func _input(event):
	if tween.is_active():
		  return
	if animation_player.is_playing():
		return
	for direction in inputs.keys ():
		if event.is_action_pressed (direction):
			facing_direction = direction
			_move (direction)
	if event.is_action_pressed('right'):
		sprite.flip_h = false
	if event.is_action_pressed('left'):
		sprite.flip_h = true
	
	if event.is_action_pressed("shoot"):
		_spawn_bullet()

func _move (direction):
	var next_tile = inputs[direction] * Global.tile_size
	ray.cast_to = next_tile
	ray.force_raycast_update ()
	if !ray.is_colliding ():
		tween.interpolate_property (self, 'position', position, position + next_tile, animation_speed, Tween.TRANS_BACK,Tween.EASE_OUT)
		tween.start()
		walk_sound.play()
		future_direction = direction
	else:
		_check_collider(ray.get_collider())

func _check_collider(collider):
	if collider != null:
		match collider.get_collision_layer():
			2: # Monster
				if health <= 1:
					_die()
				else:
					health -= 1
					print(health)
					animation_player.play ("bump")
					animation_player.play('hit')
					hit_sound.play()
			8: # Item
				print('item')
			16: # Obstacle
				animation_player.play ("bump")


func _on_Tween_tween_completed(object, key):
	emit_signal ("moved", future_direction)
	pass # Replace with function body.

func _die():
	sprite.play('death')
	die_sound.play()

func _spawn_bullet():
	ray.cast_to = inputs[facing_direction] * Global.tile_size
	ray.force_raycast_update ()
	if !ray.is_colliding ():
		shoot_shound.play()
		var bullet = bullet_scene.instance()
		bullet.position = position
		bullet._get_direction(facing_direction)
		get_owner().add_child(bullet)


func _on_Die_finished():
	Transitions.fade_out(get_owner(), "res://scenes/GridTest.tscn", 1.5, Color.white)
	pass # Replace with function body.
