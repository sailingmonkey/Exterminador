extends Camera2D


# Called when the node enters the scene tree for the first time.
func _ready():
	var rect = get_owner().get_owner().get_node("TileMap").get_used_rect()
	var cellSize = get_owner().get_owner().get_node('TileMap').cell_size
	limit_left = rect.position.x * cellSize.x
	limit_right = rect.end.x * cellSize.x
	limit_top = rect.position.y * cellSize.y
	limit_bottom = rect.end.y * cellSize.y
	pass # Replace with function body.
