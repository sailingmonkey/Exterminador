extends KinematicBody2D

export (float) var animation_speed = 0.3

export (int) var damage = 1

var directions = {"right": Vector2.LEFT,
					"left": Vector2.RIGHT,
					"down": Vector2.UP,
					"up": Vector2.DOWN}

var interrupt = false

onready var ray = $RayCast2D

onready var tween = $Tween

onready var animation_player = $AnimationPlayer

onready var animated_sprite = $AnimatedSprite

onready var explode_sound = $Explode

onready var walk_sound = $Walk

func _ready():
	var player = get_owner().get_node("Player")
	player.connect('moved', self, '_on_Player_moved')


func _on_Player_moved(direction):
	walk_sound.play()
	var next_tile = directions[direction] * Global.tile_size
	ray.cast_to = next_tile
	ray.force_raycast_update ()
	if !ray.is_colliding () :
		tween.interpolate_property (self, 'position', position, position + next_tile, animation_speed, Tween.TRANS_BOUNCE,Tween.EASE_OUT)
		tween.start()
		_check_collision(ray.get_collider())
	else:
		_check_collision(ray.get_collider())
		

func _check_collision(collider):
	if collider != null:
		match collider.get_collision_layer():
			2: # Monster
				animation_player.play ("bump")
			16: # Obstacle
				animation_player.play ("bump")

func _die():
	explode_sound.play()
	animated_sprite.play('death')


func _on_AnimatedSprite_animation_finished():
	if animated_sprite.animation != 'idle':
		queue_free()
	pass # Replace with function body.
